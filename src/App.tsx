import React, { useState, useEffect } from 'react';
import './App.css';
import FAQ from './components/faq';
import PageMockData from './components/pageMockData';
import { API_URL } from './utils/constants';
import { APIData } from './utils/types';

function App() {

  const [data, setData] = useState<APIData[]>([])
  const [open, setOpen] = useState<boolean>(false)

  useEffect(() => {
    fetch(API_URL).then((res: any) => res.json()).then((res: any) => {

      const newRes: APIData[] = Object.values(res)
      newRes.sort((a, b) => b.questionFrequency - a.questionFrequency)
      setData(newRes)
    })
  }, [])


  return (
    <div className="App">
      <div className='h-12 bg-gray-500 w-full'></div>
      <div>
        <PageMockData />
      </div>
      <div className='fixed right-4 bottom-4 cursor-pointer select-none bg-blue-500 text-white font-bold p-4 rounded-lg' onClick={() => setOpen(!open)}>
        FAQ
      </div>

      <div className={`${open ? "flex" : "hidden"} fixed right-20 bottom-20`}>
        <FAQ data={data} />
      </div>

    </div>
  );
}

export default App;
