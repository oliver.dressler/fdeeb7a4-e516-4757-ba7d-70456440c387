import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons';
import React, { useState, useEffect } from 'react';
import { APIData, FAQProps } from '../utils/types';


export default function FAQ(props: FAQProps) {

    const [open, setOpen] = useState<string>("")
    const [faqData, setFaqData] = useState<APIData[]>([])
    const [search, setSearch] = useState<string>("")
    useEffect(() => {
        setFaqData(props.data)
    }, [props.data])

    const [mostFaqs, setMostFaqs] = useState<APIData[]>([])

    useEffect(() => {
        setMostFaqs(faqData.slice(0, 5))
    }, [faqData])




    return (
        <div className=" rounded-lg p-2 border ">
            <input placeholder='Search...' className='h-12 p-4 w-80 border rounded-lg' onChange={(e) => setSearch(e.target.value)} />
            <h2 className='font-bold text-left pt-4 px-2 text-lg'>Top Questions</h2>
            {search === "" ?
                mostFaqs.map((faq: APIData, index: number) => {
                    return (
                        <div className={`p-2 m-1 w-80 text-left break-normal cursor-pointer   ${open === faq.question ? "bg-blue-100" : "bg-white"}`} key={faq.question} >
                            <h1 className='font-bold text-sm' onClick={() => open === faq.question ? setOpen("") : setOpen(faq.question)}># {faq.question}</h1>
                            <div className={`${open === faq.question ? "flex" : "hidden"} flex-col`}>
                                <p className={`text-xs `}>{faq.answer}</p>
                                <div className='flex flex-row w-full justify-around '>
                                    <div className=' rounded-lg flex items-center justify-around w-1/4 select-none' onClick={() => {
                                        let newFaq = [...faqData];
                                        newFaq[index].proVote += 1;
                                        setFaqData(newFaq)
                                    }}>
                                        <FontAwesomeIcon color='green' icon={faThumbsUp} />
                                        <div>{faq.proVote}</div>
                                    </div>
                                    <div className='rounded-lg flex items-center justify-around w-1/4 select-none' onClick={() => {
                                        let newFaq = [...faqData];
                                        newFaq[index].contraVote += 1;
                                        setFaqData(newFaq)
                                    }}><FontAwesomeIcon color='red' icon={faThumbsDown} />{faq.contraVote}</div>
                                </div>
                            </div>
                        </div>
                    )
                }) : faqData.map((faq: APIData, index: number) => {
                    return faq.question.includes(search) ? (
                        <div className={`p-2 m-1 w-80 text-left break-normal cursor-pointer   ${open === faq.question ? "bg-blue-100" : "bg-white"}`} key={faq.question} >
                            <h1 className='font-bold text-sm' onClick={() => open === faq.question ? setOpen("") : setOpen(faq.question)}># {faq.question}</h1>
                            <div className={`${open === faq.question ? "flex" : "hidden"} flex-col`}>
                                <p className={`text-xs `}>{faq.answer}</p>
                                <div className='flex flex-row w-full justify-around '>
                                    <div className=' rounded-lg flex items-center justify-around w-1/4 select-none' onClick={() => {
                                        let newFaq = [...faqData];
                                        newFaq[index].proVote += 1;
                                        setFaqData(newFaq)
                                    }}>
                                        <FontAwesomeIcon color='green' icon={faThumbsUp} />
                                        <div>{faq.proVote}</div>
                                    </div>
                                    <div className='rounded-lg flex items-center justify-around w-1/4 select-none' onClick={() => {
                                        let newFaq = [...faqData];
                                        newFaq[index].contraVote += 1;
                                        setFaqData(newFaq)
                                    }}><FontAwesomeIcon color='red' icon={faThumbsDown} />{faq.contraVote}</div>
                                </div>
                            </div>
                        </div>
                    ) : null
                })}
        </div>
    );
}

