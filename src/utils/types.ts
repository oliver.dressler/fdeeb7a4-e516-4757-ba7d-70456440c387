export interface APIData {
    question: string,
    answer: string,
    proVote: number,
    contraVote: number,
    questionFrequency: number,
    lang: string
}

export interface FAQProps {
    data: APIData[]
}